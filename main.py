import csv
import sys

from pipeline import Trip, gen_trips_from_files
from pipeline import calculate_average_distance
from pipeline import TripPipeline


def print_average(files):
    """
    Computes the average of all taxi trips from files.

    # Arguments
        files: a list of filenames
    """
    tripIterator = gen_trips_from_files(files)
    print(calculate_average_distance(tripIterator))


def print_pipeline(files):
    """
    A simple executable demonstration of the TripPipeline.
    It is set to only save 45 days of data and prints every 100000 steps
    the average of all trips.

    # Arguments
        files: a list of filenames
    """
    tripIterator = gen_trips_from_files(files)
    pipeline = TripPipeline(45)
    i = 0
    for trip in tripIterator:
        # The tlc-dataset contains several unordered taxi trips.
        # The following line prevents a major increase of time through outliers
        if i > 0 and (trip.date - pipeline._startDate).days > 45:
            continue
        pipeline.accept(trip)
        i += 1
        if i % 100000 == 0:
            # Print current average
            print(pipeline.calculate_average_distance())

    print(pipeline.calculate_average_distance())


if __name__ == "__main__":
    # print_average(sys.argv[1:])
    print_pipeline(sys.argv[1:])
