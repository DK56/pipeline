import pandas as pd
import math
from datetime import datetime


class Trip:
    """
    A representation of a taxi-trip. It does not contain unnecessary
    data from the CSV-file.
    """

    # Coloumn-names in CSV-file
    TRIP_DISTANCE = 'trip_distance'
    PICKUP_DATETIME = 'tpep_pickup_datetime'
    # Date-format used in CSV-File
    DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'

    def __init__(self, date, distance):
        self.date = date
        self.distance = distance

    @classmethod
    def from_csv_row(cls, row):
        # Deletes time information from datetime-object
        return cls(row[0].date(), row[1])


def gen_trips_from_files(fileList):
    """
    A Generator for trips. It reads and stores a small chunk of trips.

    # Arguments
        fileList: a list of files to read
    """
    for path in fileList:
        # read_csv reads chunks of 10000-trips consequetively
        for df in pd.read_csv(
                path,
                usecols=[Trip.PICKUP_DATETIME, Trip.TRIP_DISTANCE],
                iterator=True, chunksize=10000):

            # Parse/Validate the current DataFrame
            df = _parse_trip_df(df)

            # Transform it into a list of python-tuples
            chunk = df.values.tolist()

            for row in chunk:
                yield Trip.from_csv_row(row)


def _parse_trip_df(df):
    """
    Parses and validates a given DataFrame

    # Arguments
        df: DataFrame to process
    """
    # Create Datetime-objects
    df[Trip.PICKUP_DATETIME] = pd.to_datetime(
        df[Trip.PICKUP_DATETIME],
        format=Trip.DATETIME_FORMAT)

    # distance -> float-type
    df[Trip.TRIP_DISTANCE] = df[Trip.TRIP_DISTANCE].astype(float)

    # Drop all trips with NaN-distance
    df = df[df[Trip.TRIP_DISTANCE].notna()]
    # Drop all trips with distance of zero or lower
    df = df[df[Trip.TRIP_DISTANCE] > 0]
    return df
