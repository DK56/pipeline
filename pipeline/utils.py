
def calculate_average_distance(tripIterator):
    """
    A function to calculate the average distance of all trips in tripIterator.

    # Arguments
        tripIterator: iterator, which contains Trip-instances.
    """
    distanceSum = 0
    i = 0
    for trip in tripIterator:
        # Sum all distances
        distanceSum += trip.distance
        i += 1
    return distanceSum / i
