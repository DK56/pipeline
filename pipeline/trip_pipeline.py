import datetime


class TripPipeline:
    """
    A class to consume Trip-instances and save average-distances per day.
    It saves n-days of trip-data in two separate circular buffers.
    The first saved day is always '_startDate'. After n-days all trips
    on '_startDate' get removed.

    # Arguments
        n: amount of days to save
    """

    def __init__(self, n):
        self.n = n
        self.clear()

    def clear(self):
        """
        Resets all buffers and internal data.
        """
        self._dailyDistanceSums = []
        self._dailyTrips = []
        self._totalDistanceSum = 0
        self._totalTrips = 0

        self._startIndex = 0
        self._startDate = None

        # Prefill daily-buffers
        for i in range(self.n):
            self._dailyDistanceSums.append(0)
            self._dailyTrips.append(0)

    def accept(self, trip):
        """
        Adds trip to pipeline. Old trips might get deleted to allow storage.

        # Arguments
            trip: the taxi trip to add
        """
        if self._startDate is None:
            # Initialize startDate
            self._startDate = trip.date
            pos = self._startIndex
        else:
            # Calculate difference to startDate
            relativeDays = (trip.date - self._startDate).days
            if relativeDays < 0:
                # Ignore all past taxi trips
                return
            while relativeDays >= self.n:
                # No storage for new trip; delete old trips
                self._remove_day(self._startIndex)
                self._startDate += datetime.timedelta(days=1)
                self._startIndex += 1
                if self._startIndex == self.n:
                    self._startIndex = 0
                relativeDays -= 1

            # Calculates buffer position of relevant day
            pos = relativeDays + self._startIndex
            if pos >= self.n:
                pos -= self.n

        # Insert the trip on day pos
        self._insert_trip(trip, pos)

    def accept_generator(self, tripGenerator):
        """
        Helper function for iterators of Trip-instances. Calls accept foreach trip.

        # Arguments
            trip: the taxi trip to add
        """
        for trip in tripGenerator:
            self.accept(trip)

    def _insert_trip(self, trip, pos):
        """
        Inserts trip-information on pos in both buffers.

        # Arguments
            trip: the taxi trip to add
            pos: position of trip.date in buffer
        """
        self._dailyDistanceSums[pos] += trip.distance
        self._dailyTrips[pos] += 1
        self._totalDistanceSum += trip.distance
        self._totalTrips += 1

    def _remove_day(self, pos):
        """
        Removes an day and all related trips

        # Arguments
            pos: position of date to delete
        """
        self._totalDistanceSum -= self._dailyDistanceSums[pos]
        self._totalTrips -= self._dailyTrips[pos]
        self._dailyDistanceSums[pos] = 0
        self._dailyTrips[pos] = 0

    def calculate_average_distance(self):
        """
        Calculates the average distance of all trips, saved in this instance.
        """
        assert(self._totalTrips > 0)
        return self._totalDistanceSum / self._totalTrips
