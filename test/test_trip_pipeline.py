import unittest

from pipeline import Trip, TripPipeline
from . import create_trip


class TestTripPipeline(unittest.TestCase):

    def test_accept_overflow(self):
        pipeline = TripPipeline(3)

        a = create_trip('2018-01-01 20:20:20', 1.0)
        pipeline.accept(a)
        self.assertEqual(pipeline.calculate_average_distance(), 1.0)

        b = create_trip('2018-01-02 20:20:20', 2.0)
        pipeline.accept(b)
        self.assertEqual(pipeline.calculate_average_distance(), 1.5)

        c = create_trip('2018-01-03 20:20:20', 3.0)
        pipeline.accept(c)
        self.assertEqual(pipeline.calculate_average_distance(), 2.0)

        d = create_trip('2018-01-04 20:20:20', 4.0)
        pipeline.accept(d)
        self.assertEqual(pipeline.calculate_average_distance(), 3.0)

        e = create_trip('2018-01-05 20:20:20', 5.0)
        pipeline.accept(e)
        self.assertEqual(pipeline.calculate_average_distance(), 4.0)

        f = create_trip('2018-01-06 20:20:20', 6.0)
        pipeline.accept(f)
        self.assertEqual(pipeline.calculate_average_distance(), 5.0)

    def test_accept_same_day(self):
        pipeline = TripPipeline(2)

        a = create_trip('2018-01-01 20:20:20', 1.0)
        pipeline.accept(a)
        self.assertEqual(pipeline.calculate_average_distance(), 1.0)

        b = create_trip('2018-01-01 20:20:20', 2.0)
        pipeline.accept(b)
        self.assertEqual(pipeline.calculate_average_distance(), 1.5)

        c = create_trip('2018-01-01 20:20:20', 3.0)
        pipeline.accept(c)
        self.assertEqual(pipeline.calculate_average_distance(), 2.0)

        d = create_trip('2018-01-01 20:20:20', 4.0)
        pipeline.accept(d)
        self.assertEqual(pipeline.calculate_average_distance(), 2.5)

        e = create_trip('2018-01-02 20:20:20', 5.0)
        pipeline.accept(e)
        self.assertEqual(pipeline.calculate_average_distance(), 3.0)

        f = create_trip('2018-01-02 20:20:20', 6.0)
        pipeline.accept(f)
        self.assertEqual(pipeline.calculate_average_distance(), 21/6)

    def test_accept_previous_day(self):
        pipeline = TripPipeline(2)

        a = create_trip('2018-01-01 20:20:20', 1.0)
        pipeline.accept(a)
        self.assertEqual(pipeline.calculate_average_distance(), 1.0)

        b = create_trip('2017-01-01 20:20:20', 2.0)
        pipeline.accept(b)
        self.assertEqual(pipeline.calculate_average_distance(), 1.0)

        c = create_trip('2014-01-01 20:20:20', 3.0)
        pipeline.accept(c)
        self.assertEqual(pipeline.calculate_average_distance(), 1.0)

        d = create_trip('2012-01-01 20:20:20', 4.0)
        pipeline.accept(d)
        self.assertEqual(pipeline.calculate_average_distance(), 1.0)

        e = create_trip('2011-01-02 20:20:20', 5.0)
        pipeline.accept(e)
        self.assertEqual(pipeline.calculate_average_distance(), 1.0)

        f = create_trip('2012-01-02 20:20:20', 6.0)
        pipeline.accept(f)
        self.assertEqual(pipeline.calculate_average_distance(), 1.0)

    def test_accept_gap(self):
        pipeline = TripPipeline(4)

        a = create_trip('2018-01-01 20:20:20', 1.0)
        pipeline.accept(a)
        self.assertEqual(pipeline.calculate_average_distance(), 1.0)

        b = create_trip('2019-01-01 20:20:20', 2.0)
        pipeline.accept(b)
        self.assertEqual(pipeline.calculate_average_distance(), 2.0)

        c = create_trip('2019-02-01 20:20:20', 3.0)
        pipeline.accept(c)
        self.assertEqual(pipeline.calculate_average_distance(), 3.0)

        d = create_trip('2020-01-01 20:20:20', 4.0)
        pipeline.accept(d)
        self.assertEqual(pipeline.calculate_average_distance(), 4.0)

    def test_accept_mixed(self):
        pipeline = TripPipeline(3)

        a = create_trip('2018-01-01 20:20:20', 1.0)
        pipeline.accept(a)
        self.assertEqual(pipeline.calculate_average_distance(), 1.0)

        b = create_trip('2019-01-01 20:20:20', 2.0)
        pipeline.accept(b)
        self.assertEqual(pipeline.calculate_average_distance(), 2.0)

        c = create_trip('2019-01-01 20:20:20', 3.0)
        pipeline.accept(c)
        self.assertEqual(pipeline.calculate_average_distance(), 2.5)

        d = create_trip('2020-03-01 20:20:20', 4.0)
        pipeline.accept(d)
        self.assertEqual(pipeline.calculate_average_distance(), 4.0)

        e = create_trip('2020-03-01 20:20:20', 5.0)
        pipeline.accept(e)
        self.assertEqual(pipeline.calculate_average_distance(), 4.5)

        f = create_trip('2020-03-01 20:20:20', 6.0)
        pipeline.accept(f)
        self.assertEqual(pipeline.calculate_average_distance(), 5.0)

        g = create_trip('2020-03-03 20:20:20', 7.0)
        pipeline.accept(g)
        self.assertEqual(pipeline.calculate_average_distance(), 5.5)

        g = create_trip('2020-03-04 20:20:20', 8.0)
        pipeline.accept(g)
        self.assertEqual(pipeline.calculate_average_distance(), 7.5)

    def test_accept_generator(self):
        pipeline = TripPipeline(3)

        a = create_trip('2018-01-01 20:20:20', 1.0)
        b = create_trip('2019-01-01 20:20:20', 2.0)
        c = create_trip('2019-01-01 20:20:20', 3.0)
        d = create_trip('2020-03-01 20:20:20', 4.0)
        e = create_trip('2020-03-01 20:20:20', 5.0)
        f = create_trip('2020-03-01 20:20:20', 6.0)

        iterator = [a, b, c, d, e, f]
        pipeline.accept_generator(iterator)
        self.assertEqual(pipeline.calculate_average_distance(), 5.0)

        pipeline.accept_generator(iterator)
        self.assertEqual(pipeline.calculate_average_distance(), 5.0)

        pipeline.accept_generator(iterator)
        self.assertEqual(pipeline.calculate_average_distance(), 5.0)

        pipeline.accept_generator(iterator)
        self.assertEqual(pipeline.calculate_average_distance(), 5.0)

    def test_clear(self):
        pipeline = TripPipeline(3)

        a = create_trip('2018-01-01 20:20:20', 1.0)
        b = create_trip('2019-01-01 20:20:20', 2.0)
        c = create_trip('2019-01-01 20:20:20', 3.0)
        d = create_trip('2020-03-01 20:20:20', 4.0)
        e = create_trip('2020-03-01 20:20:20', 5.0)
        f = create_trip('2020-03-01 20:20:20', 6.0)

        iterator = [a, b, c, d, e, f]
        pipeline.accept_generator(iterator)
        pipeline.clear()
        self.assertEqual(pipeline._totalTrips, 0)
        self.assertEqual(pipeline._totalDistanceSum, 0)

    def test_insert_trip(self):
        pipeline = TripPipeline(3)

        a = create_trip('2018-01-01 20:20:20', 1.0)
        b = create_trip('2019-01-01 20:20:20', 2.0)
        c = create_trip('2019-01-01 20:20:20', 3.0)
        d = create_trip('2020-03-01 20:20:20', 4.0)
        e = create_trip('2020-03-01 20:20:20', 5.0)
        f = create_trip('2020-03-01 20:20:20', 6.0)
        pipeline.accept(a)
        self.assertEqual(pipeline.calculate_average_distance(), 1.0)
        pipeline._insert_trip(b, 0)
        self.assertEqual(pipeline.calculate_average_distance(), 1.5)
        pipeline._insert_trip(c, 0)
        self.assertEqual(pipeline.calculate_average_distance(), 2.0)
        pipeline._insert_trip(d, 0)
        self.assertEqual(pipeline.calculate_average_distance(), 2.5)
        pipeline._insert_trip(e, 0)
        self.assertEqual(pipeline.calculate_average_distance(), 3.0)
        pipeline._insert_trip(f, 0)
        self.assertEqual(pipeline.calculate_average_distance(), 21/6)
        pipeline._insert_trip(f, 0)
        self.assertEqual(pipeline.calculate_average_distance(), 27/7)
        pipeline._insert_trip(f, 0)
        self.assertEqual(pipeline.calculate_average_distance(), 33/8)
        pipeline._insert_trip(f, 0)
        self.assertEqual(pipeline.calculate_average_distance(), 39/9)
        pipeline._insert_trip(b, 1)
        self.assertEqual(pipeline.calculate_average_distance(), 41/10)
        pipeline._insert_trip(b, 2)
        self.assertEqual(pipeline.calculate_average_distance(), 43/11)
        pipeline._insert_trip(b, 0)
        self.assertEqual(pipeline.calculate_average_distance(), 45/12)
        pipeline._insert_trip(b, 0)
        self.assertEqual(pipeline.calculate_average_distance(), 47/13)
        pipeline._insert_trip(b, 0)
        self.assertEqual(pipeline.calculate_average_distance(), 49/14)

    def test_remove_day(self):
        pipeline = TripPipeline(6)

        a = create_trip('2019-01-01 20:20:20', 1.0)
        b = create_trip('2019-01-01 20:20:20', 2.0)
        c = create_trip('2019-01-01 20:20:20', 3.0)
        d = create_trip('2019-01-02 20:20:20', 4.0)
        e = create_trip('2019-01-02 20:20:20', 5.0)
        f = create_trip('2019-01-03 20:20:20', 6.0)

        iterator = [a, b, c, d, e, f]
        pipeline.accept_generator(iterator)
        self.assertEqual(pipeline.calculate_average_distance(), 21/6)
        pipeline._remove_day(0)
        self.assertEqual(pipeline.calculate_average_distance(), 5.0)
        pipeline._remove_day(1)
        self.assertEqual(pipeline.calculate_average_distance(), 6.0)


if __name__ == '__main__':
    unittest.main()
