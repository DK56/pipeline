import unittest
import pandas as pd
import math

from pipeline import Trip
from pipeline.trip import _parse_trip_df
from . import create_trip, create_df


class TestTrip(unittest.TestCase):

    def test_parse_datetime(self):
        df = create_df('2018-011 20:20:20', '3')
        with self.assertRaises(Exception):
            _parse_trip_df(df)

        df = create_df('2018-01-01 220:20', '3')
        with self.assertRaises(Exception):
            _parse_trip_df(df)

        df = create_df('2018--01 20:2:20', '3')
        with self.assertRaises(Exception):
            _parse_trip_df(df)

        df = create_df('2018-001-01 20:20:0', '3')
        with self.assertRaises(Exception):
            _parse_trip_df(df)

        df = create_df('2018-01-01 30:20:30', '3')
        with self.assertRaises(Exception):
            _parse_trip_df(df)

    def test_parse_distance(self):
        df = create_df('2018-01-11 20:20:20', 'a')
        with self.assertRaises(ValueError):
            _parse_trip_df(df)

        df = create_df('2018-01-11 20:20:20', '1.0')
        df = _parse_trip_df(df)

        df = create_df('2018-01-01 20:20:20', math.nan)
        df = _parse_trip_df(df)
        self.assertTrue(df.empty)

        df = create_df('2018-01-01 20:20:20', math.inf)
        df = _parse_trip_df(df)

        df = create_df('2018-01-01 20:20:30', -1)
        df = _parse_trip_df(df)
        self.assertTrue(df.empty)


if __name__ == '__main__':
    unittest.main()
