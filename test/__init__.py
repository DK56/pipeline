from datetime import datetime
import pandas as pd

from pipeline import Trip


def create_trip(dateString, distance):
    return Trip(datetime.strptime(
        dateString, Trip.DATETIME_FORMAT).date(), distance)


def create_df(dateString, distance):
    data = {Trip.PICKUP_DATETIME:  [dateString],
            Trip.TRIP_DISTANCE: [distance]}

    return pd.DataFrame(
        data, columns=[Trip.PICKUP_DATETIME, Trip.TRIP_DISTANCE])
